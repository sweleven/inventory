<div align="center" style="font-size: 7em">

<img src="https://img.icons8.com/emoji/192/000000/card-file-box-emoji.png"/>
</div>

<div align="center">
<h1>Inventory</h1>
</div>

<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_inventory"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_inventory&metric=alert_status" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_inventory"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_inventory&metric=sqale_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_inventory"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_inventory&metric=security_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_inventory"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_inventory&metric=reliability_rating" alt="quality gate"></a>
<a href="https://gitlab.com/sweleven/inventory/-/pipelines"><img src="https://gitlab.com/sweleven/inventory/badges/master/pipeline.svg" /></a>
</p>
<hr>


## Description

Microservice for rooms and workspaces management of [BlockCOVID](https://sweleven.gitlab.io/blockcovid/)  built with <a href="https://nestjs.com/">nestjs</a>.

## Docs
- [Docs](https://sweleven.gitlab.io/inventory)
- [Developer manual](https://sweleven.gitlab.io/blockcovid/docs/manuale-sviluppatore/backend/inventory_service/)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Filippo Pinton](https://gitlab.com/Butterneck.com)
- Website - [https://sweleven.gitlab.io](https://sweleven.gitlab.io/)

