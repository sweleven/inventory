import { IsOptional, IsPositive } from 'class-validator';
import { Type } from 'class-transformer';

export class PaginationQueryDto {
  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  limit: number;

  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  offset: number;
}
