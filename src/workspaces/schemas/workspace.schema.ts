import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema({ autoIndex: true, toJSON: { virtuals: true }, id: false })
export class Workspace extends Document {
  @ApiProperty()
  @Prop()
  name: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
  })
  room: string;

  @ApiProperty()
  @Prop()
  rfid: string;

  @ApiProperty()
  @Prop({
    default: true
  })
  enabled: boolean;

  @ApiProperty()
  available: boolean;

  @ApiProperty()
  @Prop({
    default: true
  })
  clean: boolean;
}

export const WorkspaceSchema = SchemaFactory.createForClass(Workspace);

WorkspaceSchema.virtual('available');
