import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Observable, from, combineLatest, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Booking } from 'src/model/booking.model';
import { CreateWorkspaceDto } from '../dto/create-workspace.dto';
import { SearchWorkspaceDto } from '../dto/search-workspace.dto';
import { UpdateWorkspaceDto } from '../dto/update-workspace.dto';
import { WorkspaceCleanStateDto } from '../dto/workspace-clean-state.dto';
import { Workspace } from '../schemas/workspace.schema';

@Injectable()
export class WorkspacesService {
  constructor(
    private readonly http: HttpService,
    @InjectModel(Workspace.name)
    private readonly workspaceModel: Model<Workspace>,
  ) { }

  /**
   * Creates a Workspace record in db, returns the newly created record
   *
   * @param {CreateWorkspaceDto} createWorkspaceDto
   * @return {*}  {Observable<Workspace>}
   * @memberof WorkspacesService
   */
  create(createWorkspaceDto: CreateWorkspaceDto): Observable<Workspace> {
    Logger.debug(`Creating a new Workspace ${createWorkspaceDto}`);

    // Generating object to store in db
    const newWorkspace = new this.workspaceModel(createWorkspaceDto);

    return from(newWorkspace.save());
  }

  /**
   * Returns a `Workspace` array containing all existing workspaces; `[]` if no workspaces are found
   *
   * @return {*}  {Observable<Workspace[]>}
   * @memberof WorkspacesService
   */
  findAll(searchQuery: SearchWorkspaceDto, only_dirty=false): Observable<Workspace[]> {
    Logger.debug(`Finding all workspaces`);

    const { limit, offset, room } = searchQuery;
    let { name } = searchQuery

    let workspaces: Workspace[];

    // if name is undefined set it to empty string, for regex search
    name = name === undefined ? '' : name;

    const finder = {
      name: { $regex: '.*' + name + '.*', $options: 'gi' }
    }

    if (room !== undefined && room !== '') {
      finder['room'] = room;
    }

    if (only_dirty) {
      finder['clean'] = false;
    }

    return from(
      this.workspaceModel.find(finder)
        .skip(offset)
        .limit(limit)
        .sort('name')
        .exec(),
    ).pipe(
      tap((_workspaces: Workspace[]) => (workspaces = _workspaces)),
      // Map array of workspaces to array of workspaces ids
      // map((workspaces: Workspace[]) => {
      //   return workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
      // }),
      switchMap((_workspaces: Workspace[]) =>
        this._getWorkspacesAvailabilityStates(_workspaces),
      ),
      map((availabilityState: boolean[]) => {
        Logger.log(availabilityState);

        // workspaces.available = availabilityState;
        workspaces.forEach((workspace, i) => {
          workspace.available = availabilityState[i];
        });

        return workspaces;
      }),
    );
  }
  

  /**
   * Returns a `Workspace` array containing all dirty workspaces; `[]` if no workspaces are found
   *
   * @return {*}  {Observable<Workspace[]>}
   * @memberof WorkspacesService
   */
   findAllDirty(searchQuery: SearchWorkspaceDto): Observable<Workspace[]> {
    return this.findAll(searchQuery, true);
   }

   

  /**
   * Returns a `Workspace`; `null` if no workspaces are found
   *
   * @param {string} id
   * @return {*}  {Observable<Workspace>}
   * @memberof WorkspacesService
   */
  findOne(id: string): Observable<Workspace> {
    Logger.debug(`Finding workspace by id`, id);

    let workspace: Workspace;

    return from(this.workspaceModel.findById(id).exec()).pipe(
      tap((_workspace: Workspace) => (workspace = _workspace)),
      switchMap((_workspace: Workspace) =>
        this._getWorkspaceAvailabilityState(_workspace._id),
      ),
      map((availabilityState: boolean) => {
        workspace.available = availabilityState;
        return workspace;
      }),
    );
  }

  /**
   * Returns a `Workspace`; `null` if no workspaces are found
   *
   * @param {string} id
   * @return {*}  {Observable<Workspace>}
   * @memberof WorkspacesService
   */
  findOneByRfid(rfid: string): Observable<Workspace> {
    Logger.debug(`Finding workspace by rfid ${rfid}`);
    let workspace: Workspace;

    return from(this.workspaceModel.findOne({ rfid: rfid }).exec()).pipe(
      tap((_workspace: Workspace) => (workspace = _workspace)),
      switchMap((_workspace: Workspace) =>
        this._getWorkspaceAvailabilityState(_workspace._id),
      ),
      map((availabilityState: boolean) => {
        workspace.available = availabilityState;
        return workspace;
      }),
    );
  }

  /**
   * Returns the list of workspaces in room
   *
   * @param {string} room
   * @return {*}  {Observable<Workspace[]>}
   * @memberof WorkspacesService
   */
  findByRoom(_room: string): Observable<Workspace[]> {
    Logger.debug(`Finding all workspaces in room ${_room}`);

    let workspaces: Workspace[];

    return from(this.workspaceModel.find({ room: _room }).sort('name').exec()).pipe(
      map((_workspaces: Workspace[]) => {
        Logger.log(_workspaces.length);
        workspaces = _workspaces.length === 0 ? null : _workspaces;
        Logger.log(`qui: ${workspaces}`);
        return workspaces;
      }),
      // Map array of workspaces to array of workspaces ids
      // map((workspaces: Workspace[]) => {
      //   return workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
      // }),
      switchMap((_workspaces: Workspace[]) => {
        return this._getWorkspacesAvailabilityStates(_workspaces);
      }),
      map((availabilityState: boolean[]) => {

        if (!availabilityState) {
          return [];
        }

        // workspaces.available = availabilityState;
        workspaces.forEach((workspace, i) => {
          workspace.available = availabilityState[i];
        });

        return workspaces;
      }),
    );
  }

  /**
   * Update workspace with id `id` (only if it already exists). Returns the
   * modified document
   *
   * @param {string} id
   * @param {UpdateWorkspaceDto} updateWorkspaceDto
   * @return {*}  {Observable<Workspace>}
   * @memberof WorkspacesService
   */
  update(
    id: string,
    updateWorkspaceDto: UpdateWorkspaceDto,
  ): Observable<Workspace> {
    Logger.debug(`Updating workspace ${id} with ${updateWorkspaceDto}`);
    return from(
      this.workspaceModel.findOneAndUpdate({ _id: id }, updateWorkspaceDto, {
        new: true, // Returns modified document rather than original
        upsert: false, // Does not create object if it does not exists (use @post)
      }),
    );
  }

  /**
   * Deletes a workspace (if exists) and returns it
   *
   * @param {string} id
   * @return {*}  {Observable<workspace>}
   * @memberof WorkspacesService
   */
  remove(id: string): Observable<Workspace> {
    return from(this.workspaceModel.findByIdAndDelete(id));
  }

  private _getWorkspaceAvailabilityState(id: string): Observable<boolean> {
    const current_datetime = new Date();
    return this.http
      .get(
        `http://booking:3004/bookings/workspace/${id}?start_datetime=${current_datetime}&end_datetime=${current_datetime}`,
      )
      .pipe(
        map((res) => res.data),
        map((bookings: Booking[]) => bookings.length == 0),
      );
  }

  private _getWorkspacesAvailabilityStates(
    workspaces: Workspace[],
  ): Observable<boolean[]> {
    const current_datetime = new Date();
    let availabilityStates: Observable<any>[] = [];
    let availabilityState = null;

    if (!workspaces) {
      return of(null);
    }

    workspaces.forEach((workspace) => {
      availabilityState = this.http
        .get(
          `http://booking:3004/bookings/workspace/${workspace._id}?start_datetime=${current_datetime}&end_datetime=${current_datetime}`,
        )
        .pipe(
          map((res) => res.data),
          tap((bookings: Booking[]) =>
            Logger.log(`workspace ${workspace._id}; bookings: ${bookings}`),
          ),
          map((bookings: Booking[]) => bookings.length == 0),
        );

      availabilityStates = availabilityStates.concat(availabilityState);
    });

    return combineLatest(availabilityStates);
  }

  public setCleanState(workspaceCleanStateDto: WorkspaceCleanStateDto): Observable<any> {
    return from(
      this.workspaceModel.findOneAndUpdate({ _id: workspaceCleanStateDto.workspace }, { clean: workspaceCleanStateDto.state }, {
        new: true, // Returns modified document rather than original
        upsert: false, // Does not create object if it does not exists (use @post)
      }),
    );
  }

  public deleteInRoom(roomId: string): Observable<any> {
    return this.findByRoom(roomId).pipe(
      switchMap((workspaces: Workspace[]) => {
        if (workspaces.length === 0) {
          return of(null);
        }
        return from(workspaces);
      }),
      switchMap((workspace: Workspace) => {
        if (!workspace) {
          return of(null);
        }
        return this.remove(workspace._id);
      }),
    );
  }
}
