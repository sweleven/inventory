import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Logger,
} from '@nestjs/common';
import { CreateWorkspaceDto } from '../dto/create-workspace.dto';
import { UpdateWorkspaceDto } from '../dto/update-workspace.dto';
import { ApiBody, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Workspace } from '../schemas/workspace.schema';
import { FindOneParams } from '../params/find-one.params';
import { WorkspacesService } from '../services/workspaces.service';
import { FindOneByRfidParams } from '../params/find-one-by-rfid.params';
import { SearchWorkspaceDto } from '../dto/search-workspace.dto';
import { EventPattern, Payload } from '@nestjs/microservices';
import { WorkspaceCleanStateDto } from '../dto/workspace-clean-state.dto';

@Controller('workspaces')
export class WorkspacesController {
  constructor(private readonly workspacesService: WorkspacesService) {}

  @Post()
  @ApiBody({
    required: true,
    type: CreateWorkspaceDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The created record',
    type: Workspace,
  })
  create(
    @Body() createWorkspaceDto: CreateWorkspaceDto,
  ): Observable<Workspace> {
    return this.workspacesService.create(createWorkspaceDto);
  }

  @Get()
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'room',
    required: false,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Workspace],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAll(
    @Query() searchQuery: SearchWorkspaceDto,
  ): Observable<Workspace[]> {
    Logger.debug(JSON.stringify(searchQuery));
    return this.workspacesService.findAll(searchQuery);
  }

  @Get('/dirty')
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'room',
    required: false,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Workspace],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllDirty(
    @Query() searchQuery: SearchWorkspaceDto,
  ): Observable<Workspace[]> {
    Logger.debug(JSON.stringify(searchQuery));
    return this.workspacesService.findAllDirty(searchQuery);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Workspace,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOne(@Param() { id }: FindOneParams): Observable<Workspace> {
    return this.workspacesService.findOne(id);
  }

  @Get('rfid/:rfid')
  @ApiParam({
    name: 'rfid',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Workspace,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOneByRfid(@Param() { rfid }: FindOneByRfidParams): Observable<Workspace> {
    return this.workspacesService.findOneByRfid(rfid);
  }

  @Patch(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiBody({
    required: true,
    type: UpdateWorkspaceDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The updated record',
    type: Workspace,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  update(
    @Param() { id }: FindOneParams,
    @Body() updateWorkspaceDto: UpdateWorkspaceDto,
  ): Observable<Workspace> {
    return this.workspacesService.update(id, updateWorkspaceDto);
  }

  @Delete(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The deleted workspace',
    type: Workspace,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  remove(@Param() { id }: FindOneParams): Observable<Workspace> {
    return this.workspacesService.remove(id);
  }

  @EventPattern('setWorkspaceCleanState')
  setCleanState(@Payload() workspaceCleanStateDto: WorkspaceCleanStateDto): Observable<void> {
    Logger.log(`Setting ${workspaceCleanStateDto.workspace} state to clean: ${workspaceCleanStateDto.state}`);
    return this.workspacesService.setCleanState(workspaceCleanStateDto);
  }

}
