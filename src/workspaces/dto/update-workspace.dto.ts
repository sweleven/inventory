import { Optional } from '@nestjs/common';
import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, Matches } from 'class-validator';
import { CreateWorkspaceDto } from './create-workspace.dto';

export class UpdateWorkspaceDto extends PartialType(CreateWorkspaceDto) {
  @ApiProperty()
  @Optional()
  @IsString({
    message: 'name: Workspace name must be a string',
  })
  @Matches(/.*/, {
    message: 'name: Workspace name must match {/put_regex_here/}', // TODO: set regex
  })
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  rfid: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  enabled: boolean;
}
