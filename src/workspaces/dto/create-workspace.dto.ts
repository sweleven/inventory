import { ApiProperty } from '@nestjs/swagger';
import { IsString, Matches, IsMongoId, IsNotEmpty } from 'class-validator';

export class CreateWorkspaceDto {
  @ApiProperty()
  @IsMongoId({
    message: 'room: Invalid room id',
  })
  room: string;

  @ApiProperty()
  @IsString({
    message: 'name: Workspace name must be a string',
  })
  @Matches(/.*/, {
    message: 'name: Workspace name must match {/put_regex_here/}', // TODO: set regex
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  rfid: string;
}
