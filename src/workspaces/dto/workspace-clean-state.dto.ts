import { IsBoolean, IsMongoId } from "class-validator";

export class WorkspaceCleanStateDto {
    
    @IsMongoId()
    workspace: string;

    @IsBoolean()
    state: boolean;
  }
  