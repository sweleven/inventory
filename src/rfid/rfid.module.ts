import { Module } from '@nestjs/common';
import { RfidService } from './services/rfid.service';
import {
  Workspace,
  WorkspaceSchema,
} from 'src/workspaces/schemas/workspace.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from 'src/rooms/schemas/room.schema';
import { RfidController } from './controllers/rfid.controller';
import { WorkspacesModule } from 'src/workspaces/workspaces.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Room.name, schema: RoomSchema }]),
    MongooseModule.forFeature([
      { name: Workspace.name, schema: WorkspaceSchema },
    ]),
    WorkspacesModule,
  ],
  controllers: [RfidController],
  providers: [RfidService],
})
export class RfidModule {}
