import { Controller, Get, Param } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Room } from 'src/rooms/schemas/room.schema';
import { Workspace } from 'src/workspaces/schemas/workspace.schema';
import { RfidService } from '../services/rfid.service';

@Controller('rfid')
export class RfidController {
  constructor(private readonly rfidService: RfidService) {}

  @Get(':id')
  findOne(@Param('id') id: string): Observable<Workspace> | Observable<Room> {
    return this.rfidService.findOne(id);
  }
}
