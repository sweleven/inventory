import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable, of } from 'rxjs';
import { combineLatest } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Room } from 'src/rooms/schemas/room.schema';
import { Workspace } from 'src/workspaces/schemas/workspace.schema';
import { WorkspacesService } from 'src/workspaces/services/workspaces.service';

@Injectable()
export class RfidService {
  constructor(
    @InjectModel(Room.name) private readonly roomModel: Model<Room>,
    @InjectModel(Workspace.name)
    private readonly workspaceModel: Model<Workspace>,
    private readonly workspacesService: WorkspacesService,
  ) {}

  findOne(id: string): Observable<Workspace> | Observable<Room>  {
    const room = from(
      this.roomModel
        .findOne({ rfid: id })
        .populate('workspaces')
        .populate('workspaces_total')
        .exec(),
    );

    const workspace = from(this.workspaceModel.findOne({ rfid: id }).exec());

    return combineLatest([room, workspace]).pipe(
      switchMap(([_room, _workspace]) => {

        if (_room) {
          return of(_room);
        } else if (_workspace) {
          return this.workspacesService.findOne(_workspace._id);
        } else {
          return of(null);
        }
      }),
    );
  }
}
