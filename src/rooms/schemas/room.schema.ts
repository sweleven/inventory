import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Workspace } from 'src/workspaces/schemas/workspace.schema';

@Schema({ autoIndex: true, toJSON: { virtuals: true }, id: false })
export class Room extends Document {
  @ApiProperty()
  @Prop()
  name: string;

  @ApiProperty()
  @Prop()
  rfid: string;

  @ApiProperty()
  @Prop({
    default: true
  })
  enabled: boolean;

  @ApiProperty()
  workspaces?: Workspace[];
}

export const RoomSchema = SchemaFactory.createForClass(Room);

RoomSchema.virtual('workspaces', {
  ref: 'Workspace',
  localField: '_id',
  foreignField: 'room',
});

RoomSchema.virtual('currentlyBookedWorkspaces', {
  ref: 'Workspace',
  localField: '_id',
  foreignField: 'room',
});

RoomSchema.virtual('currentlyUsedWorkspaces', {
  ref: 'Workspace',
  localField: '_id',
  foreignField: 'room',
});

RoomSchema.virtual('workspaces_total', {
  ref: 'Workspace',
  localField: '_id',
  foreignField: 'room',
  count: true,
});
