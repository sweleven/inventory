import { IsString, IsNotEmpty } from 'class-validator';

export class FindOneByRfidParams {
  // Same condition as create-room.dto.ts
  @IsString()
  @IsNotEmpty()
  rfid: string;
}
