import { IsOptional, IsPositive, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class SearchRoomDto {
  @ApiProperty()
  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  limit: number;

  @ApiProperty()
  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  offset: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  name: string;
}
