import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class CreateRoomDto {
  @ApiProperty()
  @IsString({
    message: 'name: Room name must be a string',
  })
  @Matches(/.*/, {
    message: 'name: Room name must match {/put_regex_here/}', // TODO: set regex
  })
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  rfid: string;
}
