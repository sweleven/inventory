import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, Matches } from 'class-validator';
import { CreateRoomDto } from './create-room.dto';

export class UpdateRoomDto extends PartialType(CreateRoomDto) {
  @ApiProperty()
  @IsOptional()
  @IsString({
    message: 'name: Room name must be a string',
  })
  @Matches(/.*/, {
    message: 'name: Room name must match {/put_regex_here/}', // TODO: set regex
  })
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  rfid: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  enabled: boolean;
}
