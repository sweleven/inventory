import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Logger,
} from '@nestjs/common';
import { RoomsService } from '../services/rooms.service';
import { CreateRoomDto } from '../dto/create-room.dto';
import { UpdateRoomDto } from '../dto/update-room.dto';
import { ApiBody, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Room } from '../schemas/room.schema';
import { FindOneParams } from '../params/find-one.params';
import { SearchRoomDto } from '../dto/search-room.dto';
import { FindOneByRfidParams } from '../params/find-one-by-rfid.params';

@Controller('rooms')
export class RoomsController {
  constructor(private readonly roomsService: RoomsService) {}

  @Post()
  @ApiBody({
    required: true,
    type: CreateRoomDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The created record',
    type: Room,
  })
  create(@Body() createRoomDto: CreateRoomDto): Observable<Room> {
    return this.roomsService.create(createRoomDto);
  }

  @Get()
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Room],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAll(@Query() searchQuery: SearchRoomDto): Observable<Room[]> {
    Logger.debug(JSON.stringify(searchQuery));
    return this.roomsService.findAll(searchQuery);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Room,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOne(@Param() { id }: FindOneParams): Observable<Room> {
    return this.roomsService.findOne(id);
  }

  @Get('rfid/:rfid')
  @ApiParam({
    name: 'rfid',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Room,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOneByRfid(@Param() { rfid }: FindOneByRfidParams): Observable<Room> {
    return this.roomsService.findOneByRfid(rfid);
  }

  @Patch(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiBody({
    required: true,
    type: UpdateRoomDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The updated record',
    type: Room,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  update(
    @Param() { id }: FindOneParams,
    @Body() updateRoomDto: UpdateRoomDto,
  ): Observable<Room> {
    return this.roomsService.update(id, updateRoomDto);
  }

  @Delete(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The deleted room',
    type: Room,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  remove(@Param() { id }: FindOneParams): Observable<Room> {
    return this.roomsService.remove(id);
  }
}
