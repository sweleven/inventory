import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Observable, from, combineLatest, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Workspace } from 'src/workspaces/schemas/workspace.schema';
import { WorkspacesService } from 'src/workspaces/services/workspaces.service';
import { CreateRoomDto } from '../dto/create-room.dto';
import { SearchRoomDto } from '../dto/search-room.dto';
import { UpdateRoomDto } from '../dto/update-room.dto';
import { Room } from '../schemas/room.schema';

@Injectable()
export class RoomsService {
  constructor(
    @InjectModel(Room.name) private readonly roomModel: Model<Room>,
    @InjectModel(Workspace.name) private readonly workspaceModel: Model<Workspace>,
    private readonly workspacesService: WorkspacesService,
  ) { }

  /**
   * Creates a Room record in db, returns the newly created record
   *
   * @param {CreateRoomDto} createRoomDto
   * @return {*}  {Observable<Room>}
   * @memberof RoomsService
   */
  create(createRoomDto: CreateRoomDto): Observable<Room> {
    Logger.debug(`Creating a new Room ${createRoomDto}`);

    // Generating object to store in db
    const newRoom = new this.roomModel(createRoomDto);

    return from(newRoom.save());
  }

  /**
   * Returns a `Room` array containing all existing rooms; `[]` if no rooms are found
   *
   * @return {*}  {Observable<Room[]>}
   * @memberof RoomsService
   */
  findAll(searchQuery: SearchRoomDto): Observable<Room[]> {
    Logger.debug(`Finding all rooms`);
    let tmp: Room[];

    const { limit, offset } = searchQuery;
    let { name } = searchQuery;

    // if name is undefined set it to empty string, for regex search
    name = name === undefined ? '' : name;

    const rooms = this.roomModel
      .find({ name: { $regex: '.*' + name + '.*', $options: 'gi' } })
      .skip(offset)
      .limit(limit)
      .populate('workspaces_total')
      .sort('name')
      .exec();

    return from(rooms).pipe(
      tap((_rooms: Room[]) => tmp = _rooms),
      switchMap((_rooms: Room[]) => {
        let workspaces$: Observable<Workspace[]>[] = [];

        _rooms.forEach((_room: Room) => {
          workspaces$ = workspaces$.concat(this.workspacesService.findByRoom(_room._id.toString()));
        })

        return combineLatest(workspaces$);
      }),
      map((workspaces: Workspace[][]) => {
        Logger.log('ciao');
        Logger.log(workspaces);
        return tmp.map((val, index) => {
          val.workspaces = workspaces[index];
          return val;
        });
      })
    );
  }

  /**
   * Returns a `Room`; `null` if no rooms are found
   *
   * @param {string} id
   * @return {*}  {Observable<Room>}
   * @memberof RoomsService
   */
  findOne(id: string): Observable<Room> {
    Logger.debug(`Finding room by id`, id);

    let tmp: Room = null;

    const room = this.roomModel
      .findById(id)
      .populate('workspaces')
      .populate('workspaces_total')
      .exec();

    return from(room).pipe(
      tap((_room: Room) => tmp = _room),
      switchMap((_room: Room) => this.workspacesService.findByRoom(_room._id.toString())),
      map((workspaces: Workspace[]) => {
        tmp.workspaces = workspaces;
        return tmp;
      }),
    );
  }

  /**
   * Returns a `Workspace`; `null` if no workspaces are found
   *
   * @param {string} id
   * @return {*}  {Observable<Workspace>}
   * @memberof WorkspacesService
   */
  findOneByRfid(rfid: string): Observable<Room> {
    Logger.debug(`Finding workspace by rfid ${rfid}`);

    let tmp: Room = null;

    const room = from(
      this.roomModel
        .findOne({ rfid: rfid })
        .populate('workspaces')
        .populate('workspaces_total')
        .exec(),
    );

    return from(room).pipe(
      tap((_room: Room) => tmp = _room),
      switchMap((_room: Room) => this.workspacesService.findByRoom(_room._id.toString())),
      map((workspaces: Workspace[]) => {
        tmp.workspaces = workspaces;
        return tmp;
      }),
    );
  }

  /**
   * Update room with id `id` (only if it already exists). Returns the
   * modified document
   *
   * @param {string} id
   * @param {UpdateRoomDto} updateRoomDto
   * @return {*}  {Observable<Room>}
   * @memberof RoomService
   */
  update(id: string, updateRoomDto: UpdateRoomDto): Observable<Room> {
    Logger.debug(`Updating room ${id} with ${updateRoomDto}`);

    let tmp: Room = null;

    const room = from(
      this.roomModel.findOneAndUpdate({ _id: id }, updateRoomDto, {
        new: true, // Returns modified document rather than original
        upsert: false, // Does not create object if it does not exists (use @post)
      }).populate('workspaces')
        .populate('workspaces_total')
        .exec()
    );


    let workspaces = of(null);

    if (updateRoomDto.enabled !== null && updateRoomDto !== undefined) {
      workspaces = from(
        this.workspaceModel.updateMany({ room: id }, { enabled: updateRoomDto.enabled}).exec()
      );
    }

    return combineLatest([room, workspaces]).pipe(
      tap(([_room, _workspaces]) => tmp = _room),
      switchMap(([_room, _workspaces]) => this.workspacesService.findByRoom(_room._id.toString())),
      map((_workspaces: Workspace[]) => {
        tmp.workspaces = _workspaces;
        return tmp;
      }),
    );

  }

  /**
   * Deletes a room (if exists) and returns it
   *
   * @param {string} id
   * @return {*}  {Observable<room>}
   * @memberof RoomsService
   */
  remove(id: string): Observable<Room> {
    Logger.debug(`Removing room ${id}`);
    return this.workspacesService.deleteInRoom(id).pipe(
      switchMap(() => from(this.roomModel.findByIdAndDelete(id))),
    );
  }
}
