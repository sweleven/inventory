import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { RoomsModule } from './rooms/rooms.module';
import { WorkspacesModule } from './workspaces/workspaces.module';
import { RfidModule } from './rfid/rfid.module';
import * as amqp_plugin from '@butterneck/mongoose-amqplib-plugin';

@Module({
  imports: [
    // TODO: configure ConfigModule:
    //    - concept variables separation
    //    - improve validation
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        PORT: Joi.number().required(),
        MONGO_USER: Joi.string().required(),
        MONGO_PASSWORD: Joi.string().required(),
        MONGO_HOST: Joi.string().required(),
        MONGO_PORT: Joi.number().required(),
        MONGO_DB: Joi.string().required(),
      }),
    }),
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        // Mongodb uri composition using env variables
        uri: `mongodb://${configService.get('MONGO_USER')}:${configService.get(
          'MONGO_PASSWORD',
        )}@${configService.get('MONGO_HOST')}:${configService.get(
          'MONGO_PORT',
        )}/${configService.get('MONGO_DB')}`,

        // Enabling amqp model publishing on save and remove
        connectionFactory: (connection) => {
          const rabbitmq_user = configService.get('RABBITMQ_USER');
          const rabbitmq_password = configService.get('RABBITMQ_PASSWORD');
          const rabbitmq_host = configService.get('RABBITMQ_HOST');
          const rabbitmq_port = configService.get('RABBITMQ_PORT');
          const domain_events_queue_name = configService.get('DOMAIN_EVENTS_QUEUE_NAME');

          // eslint-disable-next-line @typescript-eslint/no-var-requires
          connection.plugin(amqp_plugin, {
            url: `amqp://${rabbitmq_user}:${rabbitmq_password}@${rabbitmq_host}:${rabbitmq_port}`,
            queue: domain_events_queue_name,
          });
          return connection;
        },
      }),
    }),
    RoomsModule,
    WorkspacesModule,
    RfidModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
